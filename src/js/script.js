/* Consumo de API Rest mediante async await
Petición a OMB haciendo la busqueda mediante el titulo de la pelicula y nos traerá los resultados de la API */
import 'regenerator-runtime/runtime';
import Swal from 'sweetalert2';

const apiKey = "4d82de2f";
const $buscar = document.getElementById('buscar');
const $enviar = document.getElementById('enviar');
const $info = document.getElementById('informacion');
const $icon = document.getElementById("icon");
let search = '';

$enviar.addEventListener('click', e => {
    e.preventDefault();
    if($buscar.value != ""){        
        search = $buscar.value;
        realizarPeticion();
    } else{
        swalRefact('', 'Introduzca una película, genero..', 'warning')
    }
});

/**
 * realizarPeticion Función para realizar petición HTTP
 */
async function realizarPeticion() {
    try {
        loader();
        const res = await fetch(`https://www.omdbapi.com/?s=${search}&apikey=${apiKey}&`);
        const data = await res.json();
    
        let resultado = data.Search;
        let datos = '';
    
        resultado.forEach(element => {
            datos += `
                <section>
                    <img src=${ImageByDefect(element.Poster)}>
                    <h1>${element.Title}</h1>
                    <p>${element.Year}</p>
                </section>
            `;
        });
    
        $info.innerHTML = datos;
        $buscar.value = '';

    } catch (error) {
        swalRefact('Error!', 'No se  encontro la búsqueda', 'error');
        $buscar.value = '';
    }
};

/**
 * Evento para asigar modo oscuro o claro
 */
$icon.addEventListener( 'click', ()=> {
    document.body.classList.toggle("dark-mode");
    let modo = document.body.classList.contains("dark-mode");

    return (modo) ? $icon.src = "img/sun.png" : $icon.src = "img/moon.png";
});

/**
 * ImageByDefect Función para colocar una imagen por defecto cuando no tiene 
 * */
function ImageByDefect(element) {
    let newImagePoster = "img/favicon.ico";
    return (element !== "N/A") ? element : newImagePoster;
}

/**
 * loader Función para simular un loader
 */
function loader(){
    const body = document.querySelector('body');
    const imagen = document.createElement('img');
    imagen.src = 'img/loader.gif';
    imagen.className = 'loader';
    body.append(imagen);

    setTimeout( () => {
        body.removeChild(imagen)
    },200);
}

/**
 * swalRefact Función para refactorizar código de la librería Swal que se repite
 * @param {*} title 
 * @param {*} text 
 * @param {*} icon 
 */
function swalRefact(title, text, icon){

    Swal.fire({
        title: `${title}`,
        text: `${text}`,
        icon: `${icon}`,
        confirmButtonText: 'salir'
    });
}